init:
	if [ -d "public" ]; then rm -rf "public"; fi
	git clone git@codeberg.org:fediverse-tourguide/pages.git "public"
	git config core.hooksPath .githooks

publish:
	sh .githook/pre-push
	
preview:
	/usr/local/bin/hugo server -D --disableFastRender &
	sleep 1
	open http://localhost:1313

.PHONY: preview publish init 

build:
	/usr/local/bin/hugo

.PHONY: preview build init publish
