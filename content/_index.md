---
title: "Start"
date: 2020-05-24T17:52:31+02:00
draft: false
menu: main
weight: 10
images: [ tourguide_umbrella.png ]

---

# Tourguide

{{< imgr "tourguide_umbrella.png" "300px" "Lächelnde Reiseführerin mit blauem Haar, orangem Schal, roter Hose und freundlichen Glubschaugen hält einen bunten Regenschirm. Sie winkt dich weiter ...">}}

*Eine geführte Reise für neugierige Nasen durch's ganz & gar erstaunliche Universum {{<  abbr föderierender "sich verbünden,  [von lat. foedus, Gen. foederis »Bündnis, Vertrag«]"  >}}, sozialer Netzwerke.*



## Hast du dich auch schon gefragt ...

* ... ob es eine wirklich gute Idee ist, dein ganzes Privatleben global operierenden Unternehmen auf immer und ewig anzuvertrauen?

* ... ob es nicht etwas kurzsichtig ist, eigene Inhalte auf Plattformen zu publizieren, welche dir jederzeit den Zugang sperren können? 

* ... wieso fragwürdige „Soziale Netzwerke“ so alternativlos erscheinen?

* ... warum es nicht möglich sein sollte, Privatsphäre, Selbstbestimmtheit und „soziales Netzwerken“ unter einen Hut zu bringen?


{{< button2 "../010-fediverse" "Ja das interessiert mich ..." "../015-bye" "Nein nicht wirklich ...">}} 

