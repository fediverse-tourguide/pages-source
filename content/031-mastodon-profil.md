---
title: "Mastodon Benutzerprofil vervollständigen"
date: 2020-06-15T19:13:01+02:00
draft: false
---


In der linken Spalte unterhalb des Suchen-Fensters ist dein noch unvollständiges Profil zu sehen. 

{{< img "mastodon-profil-bearbeiten.png" "300px" "Bildschirmabzug: Eingerahmt der Link zu „Profil bearbeiten“">}}

---


Nach einem Klick auf {{< menu "Profil bearbeiten" >}}, kannst du 

* deinen *Anzeigenamen* festlegen und auch jederzeit wieder ändern. Bitte beachte aber, dass fediverse.at nicht für Anonyme Accounts gedacht ist! 
* eine kleine Biographie erstellen (*Über mich*)
* ein Profilbild auswählen

Weiters empfehlen wir die Option *Dieses Profil im Profilverzeichnis zeigen* zu aktivieren.


Wenn du diese drei Punkte erledigt hast unterscheidet sich dein Account schon deutlich von einem allfälligen {{< abbr "Spambot" "Maschinell angelegte Konten in sozialen Netzwerken, welche automatisiert Nachrichten verschicken um Werbung zu machen, Unruhe zu stiften oder die öffentliche Meinung zu manipulieren.">}} und wird damit auch eher ernst genommen.


{{< Hinweis "Alles weitere (Titelbild, Tabellenfelder, ...) kann später nachgetragen werden.">}}

Damit ist dein Account vollständig und du kannst loslegen. Klicke in der Mastodon App links oben auf {{< menu "< Zurück zu Mastodon" >}} und schon geht's los mit dem „Tröten“.

{{< button "../032-mastodon-troet" "Tröten ..." >}}
