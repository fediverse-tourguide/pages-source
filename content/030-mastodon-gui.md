---
title: "Mastodon Oberfläche"
date: 2020-06-15T17:35:33+02:00
draft: false
---

Nach erfolgreicher Anmeldung mit dem soeben erstellten Kontodaten siehst du im Browser ein Fenster ähnlich diesem hier:

{{< img mastodon-gui-010.png "100%" "Bildschirmabzug der Mastodonoberfläche (beispielhaft): 4 Spalten - Menü, Startseite, Mitteilungen, Erste Schritte">}}

{{< Anmerkung "Falls sich deine Ansicht von dem oben abgebildeten Screenshot grundlegend unterscheidet, bekommst du hier [Hilfe zur Mastodon Ansicht](../035-mastodon-ansicht-hilfe)" >}}

## Die Oberflächenelemente im Überblick

Schauen wir uns die drei Spalten **von links nach rechts** kurz an.

* **Benutzerbereich**: Hier kannst du nach Beiträgen und Benutzern suchen, das eigene Profil anpassen und Nachrichten ("Tröts") veröffentlichen. 

* **Startseite**: Hier wird sich später das meiste abspielen. Die Startseite zeigt dir gesammelt alle Beiträge an, welche du in der einen oder anderen Form abonniert hast. Am Anfang ist diese entweder noch leer oder du siehst die Nachrichten von einigen wenigen Leuten, denen du auf Grund von Voreinstellungen deiner Instanz folgst.

* **mastodon Menü**: Von hier aus könntest du dich über die Timelines und anderes ins Geschehen stürzen. Zuvor solltest du aber noch dein Profil vervollständigen.       

{{< button "../031-mastodon-profil" "Profil vervollständigen ...">}}

