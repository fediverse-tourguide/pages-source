---
title: "Vision"
menutitle: "Vision"
date: 2022-12-04
draft: false
menu: main
weight: 91

---



## Was wir mit `fediverse.at` erreichen wollen


`fediverse.at` versteht sich als Sammelpunkt für Menschen, welche „Soziale Dienste“ („social media“) verwenden möchten, ohne dabei auf demokratiegefährdende, zentral gesteuerte Anbieter wie Facebook, Twitter oder YouTube angewiesen zu sein.

{{< imgr "tourguide_umbrella.png" "300px" "Lächelnde Reiseführerin mit blauem Haar, orangem Schal, roter Hose und freundlichen Glubschaugen hält einen bunten Regenschirm. Sie winkt dich weiter ...">}}


Wir wollen ein Ort der Begegnung für möglichst jedermensch sein – also auch für jene, welche weder technisch noch sonst wie speziell erfahren sind.

### Anfängerfreundlich

Die **leichte Benutzbarkeit** steht an erster Stelle. Wir wollen für jeden im Umlauf befindlichen Dienst eine Instanz hier anbieten, welche sorgfältig gepflegt und dokumentiert sein soll. Diese Dienste müssen nicht zwangsläufig mit allen theoretisch möglichen Features aufwarten. Im Zweifelsfall entscheiden wir uns immer für die einfachere wenn leichter zu bedienende Variante. Auf Dienste die wir nicht in einer anfängerfreundlichen Form anbieten können, verzichten wir lieber. Wir betreiben hier **keine Freakshow** – das machen andere besser.

### Lokal

Wir wenden uns vor allem an Menschen mit Bezug zu Österreich – unabhängig von Wohnsitz oder Staatsbürgerschaft. Gerne richten wir auch Seiten oder Gruppen für bestimmte Orte oder Bundesländer ein. Die Standard-Sprache auf allen Diensten ist Deutsch. Das gilt selbstverständlich auch für unsere Hilfeseiten. Unser Ziel dabei ist, Barrieren die durch englisch-sprachige Hilfen oder Menüs entstehen zu vermeiden.

### Persönlich
Unsere Dienste müssen selbstverständlich moderiert werden. Auch um die Moderation zu vereinfachen, bitten wir keine Pseudonyme zu verwenden. Es melden sich also alle fediverse.at-Benutzer mit ihrem bürgerlichen Namen an. Wer dies nicht möchte, kann gerne diese Instanzen auch nur dazu verwenden, ins Fediverse oder einen seiner Dienste hinein zu schnuppern (aber eben mit echtem Namen). Es gibt dann genügend andere Instanzen, bei denen dann ein pseudonymer (Zweit)account eröffnet werden kann.

### Inklusive

Wir bauen in unsere Benutzerschnittstellen und Hilfetexte keine ausdrücklichen oder versteckten Barrieren für bestimmte Menschengruppen ein – weder technischer noch sonstiger Art. Das Design soll möglichst **barrierefrei**  sein (Schriftgrößen, Kontraste, Alt-Texte). Die von uns bereit gestellten Texte und Bilder vermeiden alters-, geschlechts- oder andere Stereotype zu verstärken. Wir verkomplizieren unsere Texte nicht durch kreative Anhängsel und vermeiden generell, jede  heteronormative Formulierung wie „Benutzerinnen und Benutzer“. Stattdessen verwenden wir die generische Form („Benutzer“) *nur* um alle anzusprechen und ein spezifisches Maskulinum („Benutzerich“) sollten wir die Notwendigkeit sehen, ausdrücklich nur Männer ansprechen zu müssen. Männer die sich solcherart betitelt auf den Schlips getreten fühlen, dürfen sich gerne eine andere Instanz suchen – oder kurz innehalten und bedenken, dass Frauen schon sehr lange solcherart ausgezeichnet werden :-)


### Benutzerfinanziert und werbefrei *)
Die **Finanzierung** erfolgt durch die Benutzer – sie sind Mitglieder und Kunden. Unser Produkt ist ein Server und die damit verbundene Dienstleistung – nicht unsere Benutzer oder deren Daten. Daraus folgend arbeiten wir **datensparsam**, ohne Tracker und verkaufen selbstverständlich auch keine Daten weiter. Eine Kooperation oder ähnliche Verbindung mit einem nicht-föderierenden Dienst – egal wie sympathisch und klein er ist – ist ausgeschlossen.

Jede Form von personalisierter Werbung ist ausgeschlossen. 

*) Nicht-personalisierte Werbung vor allem lokaler Gewerbetreibender ist denkbar. Mit „nicht-personalisiert“ meinen wir, dass Werbung für *alle* sichtbar und gleich sein muss. 