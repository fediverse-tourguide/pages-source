---
title: "Hilfe zur Mastodon Ansicht"
date: 2020-06-24T18:13:42+02:00
---

{{< img mastodon-gui-010.png >}}

Wenn sich deine Ansicht von der hier abgebildeten **grundlegend** unterscheidet, ist es hilfreich für die weitere Tour, wenn du diesen Zustand herstellen könntest. Folgende Fälle sind bisher bekannt:

### 1. Es sind weniger als 3 Spalten zu sehen

Lösung: Vergrößere das Browserfenster ein wenig, dann tauchen sie wieder auf.


### 2. Es sind mehr als drei Spalten zu sehen

Du hast auf die **Fortgeschrittene Benutzeroberfläche** gewechselt. Diese sieht in etwa so aus: 

{{< img "mastodon-gui-advanced.png" "400px" >}}

Lösung um zurück zur 3-spaltigen Ansicht zu kommen: 

1. Öffne die *Einstellungen* im rechten Fenster 

2. Deaktiviere das Kästchen *Fortgeschrittene Benutzeroberfläche benutzen*.

3. Klicke auf {{< gui_button "ÄNDERUNGEN SPEICHERN">}}

4. Klicke links oben auf *< Zurück zu Mastodon* 

{{< button "../030-mastodon-gui" "... zurück zur Mastodon Oberfläche" >}} 