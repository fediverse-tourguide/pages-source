---
title: "Friendica Tipps & Tricks"
description: Wie man was in Friendica macht
date: 2020-12-10
draft: false
url: friendica/tipps-und-tricks
---



Die hier veröffentlichten Tipps und Tricks entstanden großteils auf der Basis von Diskussionen im Friendica Support Forum. An dieser Stelle eine großes Dankeschön an die Entwickler und Engaierten, welche sich in diesem Forum Zeit für die Fragen und Anliegen der Benutzer nehmen!

{{< Aktionsaufruf "Du hast ein neues Feature entdeckt oder etwas verstanden, das auch für andere nützlich ist? Dann schreib uns deine Erkenntnis und wir werden diese gerne hier veröffentlichen!" >}}

## Beiträge verfassen 


### Editieren

Bereits veröffentlichte Beiträge können meist auch editiert werden (*Mehr ...* → *Edit*). Ausgenommen sind Beiträge die an ein Forum gehen.

{{< Aktionsaufruf  "Auch in anderen Situationen funktioniert das Editieren manchmal nicht - noch wissen wir nicht genau woran das liegt. Wenn du diesbezüglich etwas herausgefunden hast, ergänze doch bitte [diesen Thread](https://nerdica.net/display/a85d7459-725f-68b1-a425-bee334610401).">}}

### Zitieren

Zitate schauen am besten aus, wenn diese mit BBCode und Angabe des Autors formatiert werden. Der Autor wird dabei mit einem Gleichheitszeichen getrennt in das öffnende Quote-Tag geschrieben:


```
[quote=Some Author] Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.[/quote]
Das sehe ich anders: Namens paarmal nie Tundra Doge Afrika, erhielt Diele Bar siech patrouillierendem Adorno, bindend nur falls, gen Zitze B auflegendem äst adlige Habsburg was zus Most C da Pfand sein geschimmert biegt.
```

Natürlich kann man auch mit Markdown zitieren ...

```
> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

Das sehe ich anders: Namens paarmal nie Tundra Doge Afrika, erhielt Diele Bar siech patrouillierendem Adorno, bindend nur falls, gen Zitze B auflegendem äst adlige Habsburg was zus Most C da Pfand sein geschimmert biegt.
```

... oder im BBCode den Autor auch weglassen:

```
[quote] Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. [/quote]
Das sehe ich anders: Namens paarmal nie Tundra Doge Afrika, erhielt Diele Bar siech patrouillierendem Adorno, bindend nur falls, gen Zitze B auflegendem äst adlige Habsburg was zus Most C da Pfand sein geschimmert biegt.
```
### Empfängerkreis erweitern

Immer wieder kommt es vor, dass ich ein Thema mit jemandem bespreche und sich daraus direkt oder zeitverzögert ein Beitrag von mir auf friendica ergibt. Oder man schreibt etwas, von dem man glaubt, dass es auch für Menschen, die noch nicht im Fediverse präsent sind, interessant sein könnte. Hierfür hat friendica eine sehr praktische Funktion: 

{{< img "friendica-berechtigungen.png" "600px">}}

Im Reiter *Berechtigungen* unter *CC* können E-Mail-Adressen eingetragen werden. Die Adressaten bekommen dann vom friendica Server per Elektropost deinen Beitrag als schön formatiertes HTML E-Mail mit einem Link zum Beitrag.

Wenn du **mehr als einen Empfänger** angeben willst, trenne die E-Mail Adressen einfach mit einem Beistrich.

{{< Aktionsaufruf "Mit der E-Mail Funktion machst du jedesmal auch Werbung für das Fediverse. Irgendwann werden Menschen welche du auf diesem Weg mit für sie interessanten Informationen versorgst vielleicht selber ein Konto anlegen wollen." >}}



## Auf die Beiträge anderer antworten

Grundsätzlich kannst du jeden Beitrag aber auch jeden Kommentar kommentieren. Selbstverständlich könne auch Kommentare zu Kommentaren erstellt werden - so entstehen dann die aus anderen Medien schon bekannten "Threads" (Gesprächverläufe). Friendica bildet diese je nach Theme und Endgerät auch mehr oder weniger deutlich ab. Meist geschieht das durch Einrückungen.

Durch Klick auf die Kommentar-Schaltfläche am Ende eines Beitrags oder eines anderen Kommentars öffnet sich ein kleines Editorfenster unterhalb des zu kommentierenden Textes und dort gibst du dann den Text ein. Zitate können den Lesern deines Kommentars helfen, diesen besser zu verstehen. Siehe oben.

## Likes und Teilen (sharing)

### Wie verwende ich die Daumen-Icons?

Mit dem Anklicken die beiden Daumen-Icons (👍, 👎) signalisierst du, dass du einen Beitrag "magst" oder eben nicht.

Dabei passiert folgendes: 

* Friendica-Benutzer sehen, wer diesen Beitrag mag und damit auch wieviele Personen den Beitrag mögen (bzw. nicht mögen). Auch andere Dienste, welche eine sinngemäß gleiche Funktion haben, zeigen und zählen deinen Daumen-hoch-Klick. Der dislike-Button (👎) wird aber von nur wenigen Diensten unterstützt und daher außerhalb von friendica meist ignoriert.

* Der Beitrage wird durch einen Daumen-hoch/runter-Klick in der *Neueste Aktivität*-Timeline nochmals nach oben gerückt. (Der Admin einer Instanz kann dies aber deaktivieren; d.h. nicht alle Instanzen verhalten sich diesbezüglich gleich.)

Folgendes passiert dabei **nicht**:

* Das Anklicken des "Daumen oben" Buttons erweitert *nicht* den Empfängerkreis eines Posts. Willst du, dass alle deine Kontakte den von dir "gemochten" Beitrag auch sehen, musst du zusätzlich die *Teilen*-Schaltfläche anklicken.


### Wie verwende ich  die *Teilen* Knöpfe?

Es gibt inzwischen zwei unterschiedliche Funktionen und damit Schaltflächen für das Teilen:

1. *Teilen* ("Weitersagen") ist der einfache, schnelle One-Stop-Shop um einen Beitrag an in die Timeline der eigenen Kontakte weiterzuleiten. Ein Klick und der Beitrag ist geteilt. Ein so **unverändert** geteilter Beitrag bleibt der *selbe* Beitrag. Das ist insofern von Bedeutung, als somit alle Kommentare zu dem geteilten Beitrag von allen Empfängern diese Beitrags gelesen werden können.

2. *Zitat Teilen* ("Dies zitieren und teilen"): Hier öffnet sich ein Editor-Fenster mit dem Text des zu teilenden Beitrages. Hier kannst du dem weitergeleiteten Beitrag nun einen Text voran- oder nachstellen. Selbst kleinste Änderungen, wie ein vorangestelltes: "Finde ich gut!" verändern den Origignalbeitrag und damit ist er nicht mehr der gleiche und schon gar nicht mehr der selbe. **Es handelt sich also um einen neuen, von dir erstellten Beitrag, mit dem weitergeleiteten Beitrag als Inhalt.** Konsequenter Weise wird dieser technisch nun auch so behandelt wie ein *neuer* Beitrag und die Kommentare, welche deine Follower hinzufügen scheinen nur noch unter diesem, von dir "geteilten" Beitrag auf. 

{{< Anmerkung "Für Beiträge aus der Diaspora-Welt gelten etwas andere Regeln, da Diaspora das Teilen ein und des selben Beitrags nicht kennt und immer einen neuen Beitrag erstellt." >}}


## Suchen

Die Suche in friendica erfolgt grundsätzlich über das *Suche* Feld. Dort werden je nach Suchwunsch die Begriffe in der einen oder anderen Form eingegeben

### Beiträge suchen

Friendica kann seit dem Release von 2020.09 Beiträge suchen, deren *Inhalt* eines oder mehrere Stichwörter enthält und diese über logische Operatoren (UND, ODER) auch verknüpfen.

* Leerzeichen getrennte Stichwörter, wie beispielsweise `Wandern Bergsteigen` finden alle Beiträge in denen es ums Wandern *oder* ums Bergsteigen (oder auch um beides) geht. 

* Um beispielsweise Rezepte zum Brot backen zu suchen, würde man `+rezept +brot` ins Suchfeld eintippen. Dann werden nur jene Beiträge angezeigt, in denen das Wort *rezept* UND das Wort *brot* vorkommen. Die Groß-/Kleinschreibung spielt übrigens keine Rolle.

Eine andere Methode ist das Suchen nach Beiträgen mittels **Hashtag**. Hier kann nur *ein* Hashtag eingegeben werden. Beispielsweise `#rezept` liefert alle Beiträge mit diesem Hastag. Die Suche mittels Hashtag ist oft genauer und sortiert unerwünschte Beiträge aus. So liefert beispielsweise die Suche nach den Begriffen `+brot +rezept` einen Beitrag der sich mit einem "Rezept" gegen den Ohrwurm "Ein belegtes Brot mit Schinken" befasst. Das hast du als angehender Bäcker wohl eher nicht gesucht. Die Suche nach #rezept hätte diesen Beitrag aber richtiger Weise ausgelassen. Leider kann aber wie schon erwähnt *nicht* nach `#rezept brot` gesucht werden. Auch `#rezept #brot` ist leider *nicht* möglich. Es kann also immer nur  nach genau einem Hashtag gesucht werden.

## Umziehen

Der Wechsel innerhalb des fediverse zu einer anderen Instanz oder gar einem anderen Dienst ist theoretisch möglich in der Praxis aber nicht immer einfach. 

Wir haben damit aber noch kaum praktische Erfahrungen gemacht. Ein Bericht, den wir hier verlinkt hatten ist inzwischen aus dem Netz verschwunden.

{{< Aktionsaufruf "Du bist schon einmal erfolgreich ungezogen? Gerne veröffentlichen wir hier deinen Erfahrungsbericht!" >}}
