---
title: "Ressourcen"
date: 2020-08-22T18:55:19+02:00
draft: false
---

Eine Liste an weiterführenden Links für Benutzer die mehr wissen wollen, als die [Tipps und Tricks](../tipps-und-tricks) und natürlich die Hilfe eurer Instanz hergeben.

Wir listen hier nur deutsch- oder englischsprachige Seiten auf. Die Sprache geht aus dem Titel jeweils hervor. 

## Fediverse Allgemein

Video [What is the Fediverse?](https://framatube.org/videos/watch/4294a720-f263-4ea4-9392-cf9cea4d5277)

## Für Anfänger und leicht Fortgeschrittene

Wir gehen davon aus, dass du die Einführung gelesen und die Hilfe deiner friendica-Instanz konsultiert hast.

### Videos


#### Mini-Video Tutorials

[Übersicht](https://www.youtube.com/channel/UCgDpyTThktxYwGOqy7im6JQ)

Kurz-Tutorial - Friendica: [Freundschaftsanfrage, Kontakte knüpfen auf dem selben Friendica Server](https://www.youtube.com/watch?v=uesXgO5n_Ig)

Kurz-Tutorial - Friendica: [öffentliche und private Beiträge in der Timeline schreiben](https://www.youtube.com/watch?v=nvqd0kW2KiQ)

Kurz-Tutorial - Friendica: [Den eigenen Avatar ändern](https://www.youtube.com/watch?v=OyfKpP6l9lY)

Kurz-Tutorial - Friendica: [Die richtige Zeitzone einstellen](https://www.youtube.com/watch?v=iAuTVpJDEtA)

Kurz-Tutorial - [Friendica von Englisch auf Deutsch umstellen](https://www.youtube.com/watch?v=synYj3Bxosg)

Kurz-Tutorial - Friendica: [Blocken - Einklappen und unsichtbar machen](https://www.youtube.com/watch?v=s6QxQX7thwg)

{{< Hinweis "Inzwischen gibt es auch ohne Addons einige Möglichkeiten Kontakte die man nicht mehr will zu blocken. Die in dem Tutorial beschrieben Addons sind auch nicht auf jeder Instanz installiert." >}}


#### FriendicaHowTo


[FriendicaHowTo 01 - Erste Schritte mit friendica](https://www.youtube.com/watch?v=bLrGi_T7LJI&index=1&list=PLxC_gE7tahatdpRXlu_BrUXWx63kgJujO): Erklärt die ersten Schritte: Das Anlegen eines Accounts, grundlegende Einstellungen, wie man Beiträge veröffentlicht und sich mit Kontakten ("Freunden") verbindet.

[FriendicaHowTo 03 - Kontakte gruppieren hinzufügen](https://www.youtube.com/watch?v=nMZ3KXni5G0&list=PLxC_gE7tahatdpRXlu_BrUXWx63kgJujO&index=3): Zeigt wie man Kontakte gruppiert und sich damit die Zeitleisten gefiltert anzeigen lassen kann. 

{{< Hinweis "Das folgende Videos sind schon etwas älter - die Oberflächen schauen heute anders aus, was teilweise verwirren kann." >}}

[FriendicaHowTo 02 - Kontakte hinzufügen](https://www.youtube.com/watch?v=Iryba6Q0H1Y&list=PLxC_gE7tahatdpRXlu_BrUXWx63kgJujO&index=2): Erklärt wie man mittels friendica auch Twitter, Instagram oder Youtube Accounts folgen kann.

{{< Hinweis "Das in diesem Video erwähnte Directory ist derzeit nicht vollständig. D.h. es zeigt weit weniger Kontakte und Organisationen an als es im Fediverse gibt." >}}


## Anwendungsbeispiele

Was für coole Sachen sich mit friendica basteln lassen.

[Using Friendica's email-connector](https://github.com/friendica/friendica/wiki/Using-Friendica%27s-email-connector)

{{< Hinweis "Um einen E-Mail Connector einrichten zu können, muss deine Instanz die imap-extension in PHP aktiviert haben." >}}


[RSS to Friendika](https://github.com/pafcu/RSStoFriendika)

[Friendica als Feedreader für RSS, Twitter, Youtube etc. nutzen](http://www.produnis.de/blog/?p=2366)

----

Diese Links sind eines Auswahl aus dem sehr umfangreichen [Friendica Wiki auf Github](https://github.com/friendica/friendica/wiki).

