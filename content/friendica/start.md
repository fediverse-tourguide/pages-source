---
title: "Friendica"
date: 2020-09-20
draft: false
description: "Hilfe für Anfänger"
url: /friendica/start
menu: main
weight: 50

---


Friendica ist ein sehr mächtiges Kommunikationswerkzeug - und eine große Baustelle. Eine sorgfältige Schritte-für-Schritt Anleitung damit mensch ohne  ausgeprägte IT Kenntnissen sich zurecht findet, ergibt für Friendica derzeit noch keinen Sinn.

Für alle die es dennoch versuchen wollen, möchten wir hier etwas Hilfestellung an bieten. 

{{< Aktionsaufruf "Baustellen werden schneller fertig, wenn mehr Hände mithelfen. Die Friendica-Entwicklercommunity sucht laufend Verstärkung. Als Andockpunkte bieten sich das [Supportforum](https://forum.friendi.ca/profile/helpers) oder [Github](https://github.com/friendica/friendica) an." >}}

## Ein friendica Konto anlegen

Die Vorgangsweise ist in etwa die selbe, wie beim Anlegen eines neuen Mastodon-Kontos. Man sucht sich eine Instanz aus und registriert sich dort mit einer E-Mail Adresse. Auch die Überlegungen zur Auswahl der zu dir passenden Instanz sind ähnlich wie bei Mastodon und können [hier](/mastodon-konto-anlegen#einen-mastodon-server-auswählen) nachgelesen werden.

Verzeichnis der friendica Instanzen: https://dir.friendica.social/servers

Ein Faktor, der die Auswahl etwas verkompliziert: friendica unterstützt  Plugins, welche zusätzliche Funktionen einbauen. Da aber nicht immer alle Admins alle Plugins auf ihrer Instanz installiert haben, kann es gut sein, dass du im folgenden beschriebe Features dann nicht nützen kannst. Derzeit haben wir für diese Problem leider noch keine Lösung.

{{< Aktionsaufruf "Gerne würden wir eine eigene friendica-Instanz betreiben, bei der dann auch genau jene Plugins installiert sind, die wir im folgenden beschreiben. Zur Umsetzung suchen [wir](mailto:hello@fediverse.at) noch Linux-Admins, welche sich an der Installation und Wartung beteiligen wollen.">}}

## Erste Schritte mit friendica

Wie bereits im Guide zu Mastodon beschrieben, sind deine Timelines am Anfang leer. Fediverse-Dienste wollen dir ja nicht vorschreiben oder auch nur nahelegen, welche Nachrichten du lesen möchtest. Die auf den folgenden Seiten beschriebenen Ressourcen und Antworten zu häufig gestellten Fragen helfen beim sich zurecht finden.

ℹ️ [Tipps & Tricks](../tipps-und-tricks)  

ℹ️ [Friendica Ressourcen](../ressourcen)

{{< Hinweis "Wir legen großen Wert auf die Aktualität aller hier veröffentlichten Tipps und Hinweise. Solltest du auf veraltete, irreführende  oder gar falsche Informationen stoßen, benachrichtige uns bitte!">}} 

