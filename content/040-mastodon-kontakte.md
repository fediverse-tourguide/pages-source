---
title: "Mastodon-Kontaktnetzwerk aufbauen"
date: 2020-06-24T18:13:42+02:00
draft: true
---

## Kontakte

### Wie folge ich jemanden, von dem ich nur die ID kenne?

Wenn dir jemand seine ID bzw. Adresse, beispielsweise @luise@example.social mitteilt kannst du nicht einfach in ihrem Beitrag auf Folgen klicken - weil du ja noch keinen Beitrag von Luise in deiner Timeline hast.

In diesem Fall verwendest du das Suchen Feld. Gib die Adresse ein gefolgt von der Eingabe Taste. Nach kurzer Zeit kannst du dann das Profil öffnen und dort den Folgen Knopf anklicken. 

![Mastodon Folgen Nach Suche Dialog](../static/images/mastodon-folgen-nach-suche-dialog.png)