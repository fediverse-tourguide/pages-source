---
title: "Impressum"
date: 2020-05-24T17:54:22+02:00
draft: false
menu: main
weight: 90

---

Seitenbetreiber und für den Inhalt verantwortlich: 

Ingo Lantschner    
Marchettigasse 5/10     
1060 Wien    

E-Mail: hello@fediverse.at     
Mastodon: [@ilanti@fediverse.at](https://troet.fediverse.at/@ilanti)     
Friendica: [@ilanti@nerdica.net](https://nerdica.net/profile/ilanti/profile)     

### Feedback

Feedback zu dieser Seite gerne per [E-Mail](mailto:hello@fediverse.at) oder noch besser direkt in den [Issue-Tracker](https://codeberg.org/fediverse-tourguide/pages-source/issues) auf Codeberg.


---

### Datenschutzerklärung

Bis auf weiteres verwalten wir keinerlei personenbezogene Daten, diese Seite setzte keine Cookies und es werden keine Besuche aufgezeichnet. Gesetzliche Grundlagen sind die Datenschutz-Grundverordnung (DSGVO) und das Telekommunikationsgesetz 2003 (TKG 2003)

Der Webserverbetreiber erhebt aufgrund eines berechtigten Interesses (s.Art. 6 Abs. 1 lit. f. DSGVO) Daten über Zugriffe auf die Website und speichert diese als Server-Logfiles auf dem Server der Website ab. Folgende Daten werden so protokolliert:

* Besuchte Seite
* Uhrzeit zum Zeitpunkt des Zugriffes
* Menge der gesendeten Daten in Byte
* Quelle/Verweis, von welchem du auf die Seite gelangt bist
* Verwendeter Browser
* Verwendetes Betriebssystem
* Verwendete IP-Adresse

Die Server-Logfiles werden natürlich nicht an Dritte weiter gegeben, für maximal 14 Tage gespeichert und anschließend gelöscht. Die Speicherung der Daten erfolgt aus Sicherheitsgründen, um z. B. Missbrauchsfälle aufklären zu können. bzw. Gründen der technischen Wartung und Fehleranalyse. Müssen Logfiles aus Beweisgründen aufgehoben werden sind sie solange von der Löschung ausgenommen, bis der Vorfall endgültig geklärt ist. Diese Daten werden ausschließlich auf innerhalb der EU betriebenen Servern verarbeitet.

Für weitere Fragen wenden Sie sich bitte an die im Impressum oben genannten Kontaktmöglichkeiten.
