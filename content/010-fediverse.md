---
title: "Auf den Punkt gebracht"
date: 2020-05-24T16:25:18+02:00
draft: false
weight: 10
images: [ eule.png ]
---



> *Also sag schon: "Was ist dieses Fediverse?"*

Das Fediverse ist ein Zusammenschluss **dezentraler**, sozialer Netzwerke. 
Diese föderal organisierten *social networks* sind ein bisschen so wie das Internet der „Prä-Facebook-Ära“. 
{{< imgl "eule.png" "200px" "Eule mit Brille, Doktorhut und einem Zeigestab">}}
Vergleichbar also mit dem {{< abbr WWW "World Wide Web">}} oder E-Mail. Mit dem Fediverse kehrt das Internet also in gewissem Sinne wieder zu seinen Ursprüngen zurück – allerdings diesmal mit dem Anspruch ganz normale Menschen miteinander zu vernetzen, statt eine Elite von Akademikern.

Das Fediverse ist ähnlich wie *Twitter*, *YouTube* oder *Facebook* nur **ohne zentralen Betreiber**. Stattdessen werden mehrere {{< abbr "Instanzen" "Instanz ist ein über eine URL erreichbarer Server für den jeweiligen Dienst." >}} eines Dienstes von verschiedensten Leuten oder auch Organisationen betrieben. Benutzer können sich ihren Betreiber also aussuchen – aber dennoch mit dem gesamten Fediverse in Kontakt treten.


## Und das bringt mir was?
Diese Dezentralität und dass eine föderales Netz nun einmal keinen Eigentümer haben *kann*, hat tiefgreifende Folgen:

1. Mangels Eigentümer gibt es kein „Geschäftsmodell“ und schon gar kein so fragwürdiges, wie das der großen, zentralisierten „Social Networks“.

2. Das Fediverse muss kein Geld verdienen und daher seine Benutzer weder manipulieren noch deren Daten verkaufen. 

Daraus ergibt sich für dich:

* eine **chronologische**, nicht-kuratierte **Timeline** – du legst selber fest, was du in deiner Timeline siehst!
  {{< Hinweis "Die nicht-kuratierte Timeline ist ein wesentlicher Punkt – auf den wir etwas weiter unten nochmals zurück kommen.">}} 

  
* **werbe-** und **überwachungsfrei** – keine Tracker, welche dein Verhalten zu Manipulationszwecken aufzeichnen

* Du kannst **jederzeit den Betreiber wechseln** ohne deswegen den Dienst als ganzes verlassen zu müssen. Technisch ist das ähnlich wie der Wechsel eines Internet- oder Telefonanbieters. Diese Wahlfreiheit sorgt für einen gesunden Wettbewerb zwischen den Instanzen und gibt dir deine Autonomie wieder zurück. So musst du dich nicht sorgen, von einem föderierten Dienst wegen angeblichen Fehlverhaltens ausgesperrt zu werden.


## ... und uns allen?

Abgesehen von diesen persönlichen Vorteilen sind dezentrale Dienste auch für das große Ganze wichtig.

* Bei den zentralen Diensten stellt der Betreiber die Timeline zusammen und legt damit fest, was jeder einzelne sieht und was nicht – das nennt sich verharmlosend „Personalisierung“ – erlaubt aber Manipulationen in einem bisher nicht gekannten Ausmaß.

* Der Betreiber blendet **für jede Person maßgeschneidert** nicht nur Werbeanzeigen ein sondern gestalten auch das „redaktionelle“ Umfeld passend. Langfristig lässt sich damit die Sicht bestimmen, die jeder Einzelne auf die Welt hat – und dies zwangsweise im Auftrag der *zahlenden* Kunden des Betreibers.

* Selbst wenn es einzelnen gelingen sollte, sich dieser Manipulation zu entziehen: Mit der Präsenz und den Beiträgen im dezentralen Fediverse unterstützt mensch demokratische  Strukturen. **Es ist wichtig, den zentralisierten „Social Networks“ etwas entgegen zu setzen. Denn in diesen werden die Massen manipuliert und radikalisiert. Das betrifft uns alle, weil diese Leute dann im realen Leben handeln.**
{{< Info  "Warum wir den großen Playern jetzt etwas entgegen setzen sollten erklärt Kai Bösefeldt sehr ausführlich in [Holen wir uns das Web zurück](https://link.springer.com/epdf/10.1007/s00287-020-01274-3?sharing_token=W53h9Z46kaTKKxwo-74Udve4RwlQNchNByi7wbcMAY7aUEfJBKtlH6oluCOyz7GWO85262Nu_UIHkDPKtHkEpoBD_zhx69iLGVHESrwEKag7Ud_cbE7_Qgha4PczIdceoLhB9kGeSa0blMyTpgrS3AXZQi8Lnexhq8ITut7qbXk%3D)." >}} 

Natürlich gäbe es über das Fediverse und seine Dienste noch sehr viel mehr zu erzählen. Doch die obigen Informationen sollten für den nächsten Schritt ausreichen. Und diese spannende Reise beginnt wenig überraschend mit dem Anlegen eines Accounts ...

{{< button "../020-account-anlegen" "Nächster Schritt: Einen Account anlegen" >}}