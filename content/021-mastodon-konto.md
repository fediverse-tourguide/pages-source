---
title: "Mastodon"
date: 2020-05-25T18:41:18+02:00
draft: false
weight: 21
url: mastodon-konto-anlegen
menu: main
---

{{< imgl "mastodon-papierflieger.png" "180px" "Mastodonmaskottchen mit einem Papierflieger im Rüssel">}}

Du hast dich also entschieden ein Benutzerkonto für den Micro-Blogging-Dienst Mastodon zu erstellen - eine gute Wahl, vor allem für den Anfang!

Schritt für Schritt erklären wir nun, wie du dabei am besten vorgehst.

---

## Ein Mastodon Konto anlegen

Kurze Erinnerung: Im Fediverse gibt es für jeden Dienst nicht einen sondern *mehrere* Anbieter. Das stellt unter anderem sicher, dass du dir einen Anbieter deines Vertrauens auswählen kannst.  Solltest du diesem nicht mehr vertrauen, kannst du problemlos wechseln.

Die Instanz ist Teil deiner Identität im Fediverse, die da lautet: 

<p style="text-align:center;">@dein.name@name.der.instanz</p>

Also ähnlich wie bei einer E-Mail Adresse steht links von einem @-Zeichen dein Benutzername und rechts  der Instanzenname. Um eine Fediverse-Identität von einer E-Mail Adresse unterscheidbar zu halten, wird gerne ein weiteres „@“ dem Benutzernamen vorangestellt. 

Die Identität des Autors dieser Zeilen lautet beispielsweise:

<p style="text-align:center;"> <strong>@ilanti@fediverse.at</strong>    
<br><tt>ilanti</tt> ist mein Benutzername und <tt>fediverse.at</tt> der Name meiner Instanz. 
</p>

In einem späteren Schritt zeigen wir auch, wie du jemensch bestimmten im Fediverse finden kannst oder du eine Instanz auch wechseln kannst. Doch davor zum nächsten Schritt, der Anmeldung. 


### Mastodon Server besuchen ...

Der Einfachheit halber erklären wir die restlichen Schritte an Hand unseres eigenen Mastodon Servers. Damit stellen wir sicher, dass die Anleitung genau zu dem passt, was du in deinem Browser sehen wirst. Um unseren Mastodonserver zu besuchen, musst du lediglich  auf den folgenden Link klicken:


{{< center "🌐&#8239;<a href=https://troet.fediverse.at target=_blank>troet.fediverse.at</a>" >}}

Es wird sich ein zweites Browserfenster oder Reiter öffnen, in welchem du den Anmeldeprozess starten und in drei einfachen Schritten auch abschließen kannst:

1. In dem Browserfenster von **troet.fediverse.at** klicke  {{< gui_button_invert "Konto erstellen" >}} an. 

2. Es erscheinen einige Grundregeln – mit der {{< gui_button ACCEPT >}}-Schaltfläche geht es weiter. 

3. Nach dem Ausfüllen der Felder klicke {{< gui_button "AUF WARTELISTE KOMMEN" >}} an.     
   Bitte gib als *Anzeigenamen* deinen echten Vor- und Nachnamen an.     
   Der *Profilname* kann frei gewählt werden, darf aber keine Sonderzeichen oder Umlaute enthalten. Wenn du ungültige Zeichen verwendest, siehst du einen roten Rand um das jeweilige Feld.       
   Ein ausgefüllte Anmeldemaske könnte beispielsweise so aussehen:

{{< img "mastodon-anmeldung.png" 500px>}} 

______

Nach dem Klick auf die Schaltfläche {{< gui_button "AUF WARTELISTE KOMMEN" >}} heißt es etwas Geduld aufbringen, bis dir der Server den Freischalte-Link an die von dir angegebene E-Mail-Adresse schickt. Wir bemühen uns deine Anfrage natürlich so schnell als möglich zu beantworten. Solltest du länger als 2 Tage nichts gehört haben, [kontaktiere uns]({{< ref impressum >}}) bitte! 
 

Nach Erhalt der E-Mail. kannst du aber sofort loströten. Dazu mehr im nächsten Schritt ...

{{< button "../030-mastodon-gui"  "... die Mastodon App erkunden...">}}

