---
title: "Einen Mastodon Server auswählen"
date: 2020-06-24T18:13:42+02:00
draft: true
---


TODO Thema Instanzenwechsel - ev in Blog auslagern?

{{< Hinweis "Die Begriffe *Instanz*, *Server* und *Anbieter* werden hier der Einfachheit halber synonym verwendet." >}} 

## Einen Mastodon Server auswählen

Die Hauptseite des mastodon Projektes https://joinmastodon.org verwaltet eine Liste aktiv moderierter Mastodon-Instanzen. Die relative lange Liste wird kürzer, wenn du sie nach Kategorie und Sprache filterst.

Wir empfehlen dir für die Auswahl etwas Zeit einzuplanen und folgende Aspekte zu berücksichtigen:

* Instanzen mit sehr wenigen Benutzern sind wahrscheinlich noch sehr neu und verschwinden vielleicht bald wieder. Oder sie sind schlecht gewartet. Da es für einen Neuling schwer ist zu erkennen, ob er einen Fehler gemacht hat oder einfach nur die Konfiguration der gewählten Instanz fehlerhaft war, würden wir dazu raten, eine größere, etablierte Instanz für den Anfang auszusuchen.

* Instanzen haben oft einen regionalen, thematischen oder sprachlichen Schwerpunkt. Als am Garteln interessierter Niederösterreicher ist die Wahl einer chinesischen Hacker Instanz vielleicht keine so gute Idee. Auch wenn es natürlich möglich ist als `@leo.landwirt@黑客.cn` jedem anderen im Fediverse zu folgen (und jeder dir folgen wird können) wirst du in der {{< abbr "lokalen Timeline" "Föderierte Dienste haben meist meherere Timelines: Eine in der nur lokale Nachrichten aufscheinen oder eine globale mit allen Nachrichten aller Server. Die lokale Timeline ist vor allem am Anfang wichtig, um erste Kontakte herstellen zu können.">}} kaum jemanden finden, der mit dir kommunizieren kann und will. Weiters wird der Instanzen-Admin mit ziemlicher Sicherheit keinen deutschsprachigen Support anbieten und auch die Außenwirkung einer ausg'rissenen Adresse sollte nicht unterschätzt werden.

Im Zweifelsfall also einen Server mit eher **vielen Benutzern** der Kategorie *General* und in der Sprache *Deutsch* aussuchen. Oder eben einen, der deinen Interessen und Sprachkenntnissen entspricht. 

## Registrieren ...

Mittels der über den folgenden Button verlinkten Seite joinmastodon.org suchst du dir einen passenden Server aus. Dort legst du deinen Benutzernamen sowie ein Kennwort fest und gibst eine E-Mail Adresse an.

{{< Hinweis "Die Startseite der gewählten Instanz etwas genauer anzusehen, kann sich auszahlen. So findet sich beim `troet.cafe` beispielsweise ein Hinweis, dass gmail-Adressen nicht zu Registrierung verwendet werden können. (Der Grund ist lt. Auskunft des Betreibers, dass via gmail zu viele Spam-Accounts angelegt worden sind.)" >}} 

{{< button  "https://joinmastodon.org" "Öffne joinmastodon.org ...">}}
