---
title: "Nachrichten schreiben und Beiträge veröffentlichen („Tröten“)"
date: 2022-12-04
draft: false
---

Wohl in Anlehnung an elefantöses „Getröte“ werden Nachrichten und Beiträge auf Mastodon gerne als „Tröt“ bezeichnet. Beides, sowohl Nachrichten an einzelne Empfänger als auch für alle sichtbare Beiträge, werden im gleichen Fenster geschrieben.  

Wiederum im *Benutzerbereich* der Mastodon Oberfläche (linke Spalte) findest du ein kleines Fenster, welches dich mit der Frage „Was gibt's Neues?“ auffordert, einen Tröt zu versenden – also einen Beitrag zu verfassen und zu veröffentlichen.

{{< note Hinweis "Die Screenshots (Bildschirmabzüge) dieser Seite sind noch nicht aktuell. Die TRÖT! Buttons sind neuerdings mit Veröffentlichen beschriftet. Weiters wurde der Briefumschlag durch ein @ ersetzt. Bitte dies zu berücksichtigen!" >}}




{{< img "mastodon-troet.png" "300px" "Bildschirmabzug: Das Texteingabe-Feld mit dem Textvorschlag „Was gibt's Neues?“. Darunter der Button TRÖT!">}} 

Du kannst wem auch immer damit eine Nachricht zukommen lassen – einem globalen Empfängerkreis genauso wie einer kleineren Gruppe oder auch nur einer einzelnen Person. Im folgenden Beispiel gehen wir davon aus, dass du dem Autor dieser Zeilen was tröten willst.

## Nachricht verfassen

Tippe dazu den Text in den weißen Bereich. Die Zahl unten rechts zeigt dir an, wieviele Zeichen du noch eingeben kannst. Zur Erinnerung: Mastodon ist ein Micro-Blogging Netzwerk also eher auf kurze Nachrichten ausgelegt.

Wenn du wie für dieses Beispiel vorgeschlagen die Nachricht an einen bestimmten Empfänger senden willst, musst du diesen natürlich im Text erwähnen. Im Gegensatz zu beispielsweise E-Mail gibt es dafür *kein* eigenes Feld, sondern du schreibst den Empfänger irgendwo in den Text – am Anfang, mitten in einem Satz oder auch erst am Ende.

Ein möglicher Text wäre:

```
Hallo @ilanti@fediverse.at, ich bin gut im Fediverse gelandet. Bei eurem Reiseführer auf https://tour.friendica.at ist mir aufgefallen, dass ...

Grüße, Michi 

```

##  Sichtbarkeit einstellen

Bevor du nun mit einem Klick auf den {{< gui_button "Veröffentlichen!" >}}-Button deine Nachricht veröffentlichst, möchtest du vielleicht einstellen, wer aller deine Nachricht bekommen soll. Dazu klickst du auf das kleine Weltkugelsymbol unterhalb des Textes. Dadurch öffnet sich ein Auswahlfeld:

{{< img "mastodon-sichtbarkeit-einstellen.png"  "300px" "Bildschirmabzug: Weltkugel-Symbol mit den Auswahlmöglichkeiten: Öffentlich (ausgewählt), Nicht gelistet, Nur für Folgende, Direktnachricht">}} 

Um den Tröt gezielt nur an die eine, im Nachrichtentext erwähnte Person zu senden, musst du einfach auf {{< menu "@ Nur erwähnte Profile" >}}  klicken. Damit ändert sich auch das Aussehen der Nachrichtenbox: Dort wo vorher eine Weltkugel war, ist nun ein Klammeraffe (@) abgebildet. Weiters erscheint eine kleine Infobox und der *Veröffentlichen*-Button hat ein Schloss-Symbol bekommen.

{{< img "mastodon-troet-ready.png" "300px" "Bildschirmabzug einer Textnachricht. Darunter der TRÖT-Knopf mit einem Vorhängeschloss davor.">}} 

{{< Info "Als Blogging-Netzwerk ist Mastodon vor allem auf die rasche und einfache **Veröffentlichung** von Beiträgen optimiert. Vertraulichkeitsaspekte spielen dabei keine große Rolle und so sollten persönliche Nachrichten mit vertraulichen Inhalten eher nicht via Mastodon versendet werden. Dafür gibt es weit besser geeignet Technologien wie beispielsweise verschlüsselte E-Mails oder Messenger wie Signal." >}} 

## Beitrag versenden

Der letzte Schritt ist der einfachste. Kontrolliere besser nochmals, ob die Sichtbarkeitseinstellungen passen (also ein @ zu sehen ist *statt* einer Weltkugel). Dann klickst du auf den {{< gui_button "🔒Veröffentlichen" >}}-Button und hast damit die Nachricht versendet.

---

## Wie geht's weiter?

Hier endet dieser Reiseführer für's Erste. Abhängig von den Rückmeldungen, werden wir wohl das eine oder andere Kapitel noch überarbeiten oder ergänzen. Wir denken da vor allem an die Zeitleisten.

Bis es so weit ist, dürfen wir dir noch den Blog von Kev Quirk ans Herz legen. Vor allem sein Beitrag [How Does Mastodon Work?](https://kevq.uk/how-does-mastodon-work/) wäre als weiterführende Lektüre gut geeignet, wenn du Mastodon bis ins Detail ergründen möchtest.

Wir freuen uns natürlich über [Feedback](/impressum/)!
