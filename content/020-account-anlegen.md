---
title: "Ein Benutzerkonto erstellen"
date: 2022-12-03
draft: false
weight: 20
---


{{< imgr "figur_accountanlegen_300.png" "250px" "Frau mit grün, geringeltem Pulli setht und hält ein Schild hoch: ACCOUNT ANLEGEN">}}

Mit einem Konto im Fediverse kannst du die Beiträge anderer lesen, ausgewählten Foren oder Unter&shy;haltungen folgen und selber schreiben.

Um für dich ein Konto anzulegen musst du ...

1. einen **Dienst** auswählen.
2. eine **Instanz**, also einen der Betreiber für diesen Dienst auswählen. 
3. ein **Konto** für dich auf dieser Instanz anlegen.

Das war's im Überblick – es folgt eine Schritt-für-Schritt Anleitung.



## Einen Dienst auswählen


Das Fediverse besteht aus unterschiedlichen Diensten, welche unterschiedliche Medien-Schwerpunkte haben: Es gibt Dienste für den Austausch **kurzer Nachrichten** ähnlich wie Twitter, einen Facebook-ähnlichen Dienst, einen zum Austausch von **Videos** (PeerTube) oder **Fotos** und noch viele mehr. Für den Anfang wollen wir zwei davon kurz vorstellen. So kannst du dich informiert für einen der beiden entscheiden.



### Mastodon


Mastodon ist ein **Microblogging Netzwerk**, ähnlich wie Twitter. Die Nachrichten sind eher kurz, wenn auch die 2048&#8209;Zeichen&#8209;Beschränkung für die Länge einer Nachricht schon recht großzügig dimensioniert ist. Selbstverständlich können auch Bilder, Videos und andere Dateien ausgetauscht werden – der Schwerpunkt liegt aber eben bei Kurznachrichten. Mastodon ist der aktuell mit Abstand beliebteste Dienst im Fediverse, mit über 8&#8239;Million Nutzern (Stand Dezember&#8239;2022). 

{{< imgl "mastodon.png" "100px" "Mastodonmaskottchen: Braunes Rüsseltier mit Stoßzähnen">}}
  
Als Maskottchen für das Mastodon-Netzwerk hat sich ein prähistorisches Rüsseltier, eben ein Mastodon etabliert. Eine Nachricht wird auf Mastodon konsequenter Weise „toot“ genannt. Fast noch besser ist die deutsche Übersetzung „Tröt“ – wohl davon ausgehend, dass Mastodons, wie ihre noch heute lebenden Verwandten, die Elefanten, tröten würden.


### Friendica

{{< imgl "friendica.png" "100px" "Friendica-Logo: Je ein gelbes und ein blaues, groß-geschriebenes F um 180° gedreht, ineinander verschränkt ein Quadrat mit runden Ecken ergebend.">}} 

Friendica ist ein **Macroblogging Netzwerk**, also auf längere Texte und Beiträge ausgerichtet. Es ist am ehesten mit Facebook vergleichbar – nur eben dezentral angelegt. Friendica hat viele Features, „Themes“ und Einstellmöglichkeiten, in denen man sich anfangs gerne einmal verheddert. Mit knapp 3000 aktiven Benutzern ist Friendica auch noch kein Massenphänomen.



{{< Hinweis "Neben diesen beiden Diensten gibt es noch viele andere. Der Einfachheit halber zählen wir diese hier nicht alle auf. Einen Überblick bietet dir die Seite [fediverse.party](https://fediverse.party)" >}} 


**Egal für welchen der Dienste du dich entscheidest, du kannst dich in jedem Fall mit allen Benutzern – auch denen aller anderen Dienste – vernetzen!** Also ein friendica Benutzer kann jemensch auf Mastodon folgen und umgekehrt. Und das gilt nicht nur für diese beiden Dienste sondern für alle im Fediverse.

{{< button2 "../mastodon-konto-anlegen" "Mastodon Konto anlegen (empfohlen)"
 "/friendica/start" "Friendica Konto anlegen (IT-experts only!)">}}